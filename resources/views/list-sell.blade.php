
@extends('master')

@push('title')
<title>Books Store | List Sells</title>
@endpush

@section('content')
<div class="bg-light py-3">
    <div class="container">
        <div class="row">
        	<div class="col-md-12 mb-0"><a href="{{url('/home')}}">Home</a> <span class="mx-2 mb-0">/	</span> <a href="{{url('/sell')}}">Sell</a><span class="mx-2 mb-0">/</span><strong class="text-black">List Product</strong></div>
        </div>
    </div>
</div>
<div class="site-section">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h2 class="h3 mb-3 text-black">Your Sell Products</h2>
			</div>
			<div class="col-md-10">
				<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Title</th>
						<th>Categories</th>
						<th>Description</th>
						<th>Upload at</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
				  @foreach($sell as $index)
					<tr>
					<td></td>
						<td>{{$index->Name}}</td>
						
						<td>{{$index->Description}}</td>
						<td>{{$index->Product_Image}}</td>
						<td>{{$index->Price}}</td>
					</tr>
				  @endforeach
				</tbody>
				</table>
			</div>
			<div class="col-md-2 p-3 p-lg-6 border position-sticky" style="height: 100px; position: sticky">
				<a href="{{url('/sell')}}">Add Product</a>
				<br>
				<strong class="text-black">List Product</strong>
			</div>
		</div>
	</div>	
</div>

@endsection
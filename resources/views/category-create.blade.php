@extends('master')

@push('title')
<title>Books Store</title>
@endpush

@section('content')
<div class="bg-light py-3">
	<div class="container">
		<div class="row">
			<div class="col-md-12 mb-0"><a href="{{url('/home')}}">Home</a> <span class="mx-2 mb-0">/</span><a href="{{url('/sell')}}">Sell</a><span class="mx-2 mb-0">/</span><a href="{{url('/sell/category')}}">Category</a><span class="mx-2 mb-0">/</span>  <strong class="text-black">Create</strong></div>
		</div>
	</div>
</div>
<div class="site-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="h3 mb-3 text-black">Input Category</h2>
			</div>

			<div class="col-md-5">
				<div class="p-3 p-lg-5 border">
					<form action="{{url('/sell/category')}}" method="POST">
						@csrf
						<label for="Name" class="col-form-label text-primary h6 text-uppercase">Category Name</label>
						<input type="text" id="Name" required name="Name" class="form-control">
						<br>
						<label for="Description" class="col-form-label text-primary h6 text-uppercase">Description</label>
						<input type="text" id="Description" required name="Description" class="form-control">
						<br><br>
						<input type="submit" value="submit" class="btn btn-default">
					</form>
				</div>
			</div>
		</div>

		
	</div>
</div>

@endsection
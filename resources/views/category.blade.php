@extends('master')

@push('title')
<title>Books Store</title>
@endpush

@section('content')
<div class="bg-light py-3">
	<div class="container">
		<div class="row">
            <div class="col-md-12 mb-0"><a href="{{url('/home')}}">Home</a> <span class="mx-2 mb-0">/</span><a href="{{url('/sell')}}">Sell</a><span class="mx-2 mb-0">/</span><strong class="text-black">Category</strong></div>
		</div>
	</div>
</div>
<div class="site-section">
	<div class="container">
    <a href="{{url('/sell/category/create')}}" class="btn btn-primary">Create New Category</a>
    <br><br>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Created By</th>
            </tr>
        </thead>
        <tbody>
        @foreach($category as $index)
            <tr>
                <td>{{$index->Name}}</td>
                <td>{{$index->Description}}</td>
                <td>{{$index->users_cat->username}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
	</div>
</div>

@endsection
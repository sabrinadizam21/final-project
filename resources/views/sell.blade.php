@extends('master')

@push('title')
<title>Books Store | Sell</title>
@endpush

@section('content')
<div class="bg-light py-3">
    <div class="container">
        <div class="row">
        	<div class="col-md-12 mb-0"><a href="{{url('/home')}}">Home</a> <span class="mx-2 mb-0">/	</span> <strong class="text-black">Sell</strong><span class="mx-2 mb-0">/	</span><strong class="text-black">Add Product</strong></div>
        </div>
    </div>
</div>
<div class="site-section">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h2 class="h3 mb-3 text-black">Sell Your Book</h2>
			</div>
			<div class="col-md-10">
				<form action="{{url('/sell/list')}}" method="post">
					@csrf
					<div class="p-3 p-lg-5 border">
						<div class="form-group row">
							<div class="col-md-12">
								<label for="name" class="text-black">Title<span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="name" name="name">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label for="category" class="text-black">Category<span class="text-danger">*</span>
									<a href="{{url('/sell/category')}}" class="btn btn-link text-lowercase" style="font-size: 10pt;">Add Category</a></label>
								<select class="form-control" id="category" name="category">
									<option>...</option>
									@foreach($category as $kategori)
									 <option>{{$kategori->Name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label for="product_Price" class="text-black">Price<span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="product_Price" name="product_Price">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label for="description" class="text-black">Description</label>
								<textarea name="description" id="description" cols="30" rows="7" class="form-control"></textarea>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="product_image" class="text-black">Upload Image</label>
								<input type="file" accept="image/*" name="product_image" id="file"  onchange="loadFile(event)" style="display: none;">
								<p><img id="output" width="479" height="340" /></p>

								<label for="file" style="cursor: pointer;" class="btn btn-danger">Search</label>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4">
								<input type="submit" class="btn btn-primary btn-lg btn-block" value="Sell">
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="col-md-2 p-3 p-lg-6 border" style="height: 100px;">
				<strong class="text-black">Add Product</strong>
				<br>
				<a href="{{url('/sell/list')}}">List Product</a>
			</div>
		</div>
	</div>
</div>
@endsection

@push('script')
<script>
	var loadFile = function(event) {
		var image = document.getElementById('output');
		image.src = URL.createObjectURL(event.target.files[0]);
	};
</script>
@endpush
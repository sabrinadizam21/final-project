@extends('master')

@push('title')
<title>Books Store</title>
@endpush

@section('content')
<div class="bg-light py-3">
	<div class="container">
		<div class="row">
			<div class="col-md-12 mb-0"><a href="{{url('/dashboard')}}">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Profile</strong></div>
		</div>
	</div>
</div>
<div class="site-section">
	<div class="container">
		<div class="row">
				@if ($message = Session::get('topUpSukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
		        @endif
                @if ($message = Session::get('topUpGagal'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
		        @endif
			<div class="col-md-12">
            	<h2 class="h3 mb-3 text-black">Your Account</h2>
          	</div>
          	<div class="col-md-12 p-3 p-lg-5 border">
          		<div class="container">
          			<div class="row">
          				<div class="col-sm col-md-3">
          					<picture>
          						<source srcset="{{asset('images/dp.png')}}" type="">
          					<img src="{{Illuminate\Support\Facades\Auth::user()->UsersPhoto}}" width="150px" class="rounded-circle" alt="User Image" >
          					</picture>
          					<!-- -->	
          				</div>
          				<div class="col-sm col-md-7">
          					<h1>{{Illuminate\Support\Facades\Auth::user()->username}}</h1>
          					<h4>{{Illuminate\Support\Facades\Auth::user()->name}}</h4>
          				</div>
          				<div class="col-sm">
          					<h4>Balance : {{Illuminate\Support\Facades\Auth::user()->Saldo}}</h4>
          					<button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Top Up</button>
          				</div>
          			</div>
          		</div>
          		<br>
          		<div class="col">
					<h6>{{Illuminate\Support\Facades\Auth::user()->Alamat}}</h6>
				</div>
				<button class="btn btn-link">Update Profile</button>
			</div>
			
			<!--HISTORY PEMBELIAN-->
			<div class="col-md-6 p-3 p-lg-5 border">
				<div>
					<h1 class="d-block text-primary h6 text-uppercase">Sales History</h1>
					<table class="table">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Title</th>
								<th scope="col">Upload Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">1</th>
								<td>Mark</td>
								<td>Otto</td>
							</tr>
							<tr>
								<th scope="row">2</th>
								<td>Jacob</td>
								<td>Thornton</td>
							</tr>
							<tr>
								<th scope="row">3</th>
								<td>Larry</td>
								<td>the Bird</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<!--HISTORY PENJUALAN-->
			<div class="col-md-6 p-3 p-lg-5 border">	
				<div>
					<h1 class="d-block text-primary h6 text-uppercase">Purchase History</h1>
					<table class="table">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Title</th>
								<th scope="col">Order Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">1</th>
								<td>Mark</td>
								<td>Otto</td>
							</tr>
							<tr>
								<th scope="row">2</th>
								<td>Jacob</td>
								<td>Thornton</td>
							</tr>
							<tr>
								<th scope="row">3</th>
								<td>Larry</td>
								<td>the Bird</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
				</div>
			</div>

			<!--TOP UP-->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Top Up Balance</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form action="{{url('/topupsaldo')}}" method='POST'>
								@csrf
								<div class="form-group">
									<label for="Saldo" class="col-form-label">Balance</label>
									<input type="number" id="Saldo" name="Saldo" class="form-control">
								</div>
								<div class="password">
									<label for="message-text" class="col-form-label">Your Password Account</label>
									<input type="password" id="password" name="password" class="form-control">
								</div>

						<div class="modal-footer">
							<input type="submit" value="TopUp" class="btn btn-primary">
						</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<!--UPDATE PROFILE-->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Top Up Balance</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form action="#" method='POST'>
								@csrf
								<div class="form-group">
									<label for="name" class="col-form-label">Name</label>
									<input type="text" id="name" name="name" class="form-control">
								</div>
								<div class="form-group">
									<label for="email" class="col-form-label">email</label>
									<input type="email" id="email" name="email" class="form-control">
								</div>
								<div class="password">
									<label for="message-text" class="col-form-label">Your Password Account</label>
									<input type="password" id="password" name="password" class="form-control">
								</div>

						<div class="modal-footer">
							<input type="submit" value="TopUp" class="btn btn-primary">
						</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
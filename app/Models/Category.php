<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table = "category";
    protected $fillable =
        [
            'Name',
            'Description',
            'created_at',
            'updated_at',
            'users_id'
        ];

    public function users_cat() {
        return $this->belongsTo('App\Models\User','users_id');
    }
}

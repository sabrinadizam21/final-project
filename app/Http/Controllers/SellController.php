<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\User;
use App\Models\Sell;


class SellController extends Controller
{
    public function index() {
        $category = Category::with('users_cat')->get();
        //$cat_sell = Sell::with('cat_user')->get();
        $sell = Sell::all();
        return view('sell', ['sell' => $sell, 'category' => $category ]);
        
    }
   
    public function create() {
        return view('sell');
    }

     /*public function store(request $request) {
     	$request->validate([
            "Name" => 'required|unique:product',
            "Description" => 'required',
            "Product_Image" => 'required',
            "Price" => 'required'

         ]);

        try{
            $category = Category::create([
                'Name' => $request['Name'],
                'Description' => $request['Description'],
                'Product_Image' => $request['Product_Image'],
                'Price' => $request['Price'],
                'Category_id' => $request['Category_id']
            ]);
        } catch(QueryException $a) {
            $message = $a->getMeessage();
        }
        return redirect('/sell/list');
    }
    public function sell() {
        $category = Category::all();
        return view('sell', ['category' => $category]);
     }*/
}

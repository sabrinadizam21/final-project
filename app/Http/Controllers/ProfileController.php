<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Session;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('profil');
    }

    public function topUpSaldo(request $request) {
        $request->validate([
            "Saldo" => 'required',
            "password" => 'required'
         ]);

        try{
            $saldo = Auth::user()->Saldo + $request['Saldo'];
            $password = $request['password'];

            if (Hash::check($password, Auth::user()->password)) {
                // Jika password benar
                $user = User::where('id', Auth::user()->id)->update([
                    'Saldo' => $saldo
                ]);
                Session::flash('topUpSukses','Saldo anda telah bertambah.');
            }else{
                // jika password tidak sesuai
                Session::flash('topUpGagal','Saldo tidak bisa di isi.');
            }

        } catch(QueryException $a) {
            $message = $a->getMeessage();
        }
        return redirect('/profile');
    }
}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function index() {
        $category = Category::with('users_cat')->get();
        return view('category', ['category' => $category]);
        
    }

    public function create() {
        return view('category-create');
    }

    public function store(request $request) {

        $request->validate([
            "Name" => 'required|unique:category',
            "Description" => 'required'
         ]);

        try{
            $sell = Sell::create([
                'Name' => $request['Name'],
                'Description' => $request['Description'],
                'category_id' => $request['category_id'],
            ]);
        } catch(QueryException $a) {
            $message = $a->getMeessage();
        }
        return redirect('/sell/list');
    }
    public function sell() {
        $category = Category::all();
        return view('sell', ['category' => $category]);
        
    }
}

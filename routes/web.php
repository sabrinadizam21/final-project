<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\DashboardController::class, 'index'], 301);

//Halaman Awal web
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

Route::get('/login', function(){
	return view('login');
});

Route::get('/register', function(){
	return view('register');
});



Auth::routes();
Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', [App\Http\Controllers\DashboardController::class, 'index'])->name('home');
    //PROFIL
    Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile');

    Route::resource('/sell/category', 'App\Http\Controllers\CategoryController');

    Route::POST('/topupsaldo', [App\Http\Controllers\ProfileController::class, 'topUpSaldo'])->name('topupsaldo');
    
    //DAFTAR BUKU YANG DIJUAL
    Route::get('/shop', function(){
        return view('shop');
    });

    //USER JUAL BUKU
    Route::get('/sell', [App\Http\Controllers\SellController::class, 'index'])->name('sell');

        
    Route::post('/sell/list', function(){
        return view('list-sell');
    });

    //BUKU YANG ADA DI KERANJANG
    Route::get('/cart', function(){
        return view('cart');
    });

    //CHEHKOUT
    Route::get('/checkout', function(){
	    return view('checkout');
    });

    //LAMAN BUKU PER ITEM
    Route::get('/shop-single', function(){
        return view('shop-single');
    });

});